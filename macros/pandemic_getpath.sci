function path = pandemic_getpath()
    path = get_function_path("pandemic_getpath")
    path = fullpath(fullfile(fileparts(path), ".."))
endfunction
