function pandemic_plot_deaths(selectedCountries, countries, timestamps, new_deaths)
    
    // callback mode
    if argn(2) == 0 then
        listbox = findobj("callback", "pandemic_plot_deaths");
        [countries, timestamps, new_deaths] = listbox.userdata(:);
        allCountries = unique(countries);
        selectedCountries = listbox.string(listbox.value);
    else
        allCountries = unique(countries);
        // when not specified, list all countries
        if selectedCountries == [] then
            selectedCountries = allCountries';
        else
            selectedCountries = selectedCountries(:)';
        end
    end

    // list all countries on the UI
    f = gcf();
    if type(f.children(1)) == 9 & f.children(1).type == "Axes" then
        f.layout = 'border';
        a = f.children(1);
        a.parent = uicontrol(f, 'style', 'frame');
        
        [?, ka] = intersect(allCountries, selectedCountries);
        str = allCountries(1:$-1)
        listbox = uicontrol(f, 'style', 'listbox', ..
                            'callback', 'pandemic_plot_deaths', ..
                            'position', [10 10 150 200], ..
                            'constraints', createConstraints('border', 'left'), ..
                            'max', size(str, '*'), ..
                            'string', str, ..
                            'value', ka);
        listbox.userdata = list(countries, timestamps, new_deaths);

        [Y,M,D] = datevec(max(timestamps));
        f.figure_name = msprintf("Cases on %04d-%02d-%02d", Y, M, D);
        f.color_map=rainbowcolormap(size(allCountries,'*'));
    
        a.x_label.text = "Days after more than 10 detected deaths"
        a.y_label.text = "Number of deaths"
        a.data_bounds = a.data_bounds * 1.1
        a.grid = [0 0]
        a.grid_style = [10 10];
    else
        a = f.children(2).children;
        delete(a.children);
    end

    
    for c=selectedCountries
        currentId = find(countries == c);
        
        t = timestamps(currentId);
        n = new_deaths(currentId);
        [?,kv] = gsort(t, 'g', 'i');
        
        t = t(kv);
        deaths = cumsum(n(kv));

        // start on ..
        selector = deaths > 10;
        t = t(selector);
        deaths = deaths(selector);

        if size(n, "*") then
            // zero delay filter (signal + reverse) on a 3 day window
            N = 3 + 1;
            n = filter(ones(N/2,1)/N, 1, n)+filter(ones(N/2,1)/N, 1, n($:-1:1))($:-1:1);
            
            x = t-t(1);
            y = deaths;
            plot2d(x, y, find(c == allCountries));
            xstring(x($), y($), c);
        end
    end
endfunction
