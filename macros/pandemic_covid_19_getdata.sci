function [geoids, countries, timestamps, new_cases, new_deaths] = pandemic_covid_19_getdata(save_into)
    // will retrieve data from an CSV file on ecdc.europa.eu updated daily
    //
    // Value: Real matrix, the Excel data as double
    // Text: String matrix, the Excel data as string

    fname = fullfile(TMPDIR, "csv");

    // the current file might be cached
    if isfile(fname) & part(mgetl(fname, 1), 1:7) == "dateRep" then
        [geoids, countries, timestamps, new_cases, new_deaths] = decode_csv(fname);
        if isdef("save_into", "local") then
            copyfile(fname, save_into);
        end
        return
    end

    // download the CSV file and store it if requested
    // https://opendata.ecdc.europa.eu/covid19/casedistribution/csv
    URL = "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv";
    try
        downloaded = getURL(URL, TMPDIR);
        if isfile(downloaded) & part(mgetl(downloaded, 1), 1:7) == "dateRep" then
            [geoids, countries, timestamps, new_cases, new_deaths] = decode_csv(downloaded);
            if isdef("save_into", "local") then
                copyfile(downloaded, save_into);
            end
            return
        end
    end
    
    // on download error, use the toolbox copy
    fname = fullfile(pandemic_getpath(), "data", "COVID-19-geographic-disbtribution-worldwide.csv");
    [geoids, countries, timestamps, new_cases, new_deaths] = decode_csv(fname);
endfunction

function [geoids, countries, timestamps, new_cases, new_deaths] = decode_csv(fname)
    Value = real(csvRead(fname, ",", [], "double"));
    Text = csvRead(fname, ",", [], "string");
    
    geoids = Text(:,Text(1,:) == "geoId");
    countries = Text(:,Text(1,:) == "countriesAndTerritories");
    timestamps = datenum(Value(:,Text(1,:) == "year"),Value(:,Text(1,:) == "month"),Value(:,Text(1,:) == "day"));
    new_cases = Value(:,Text(1,:) == "cases");
    new_deaths = Value(:,Text(1,:) == "deaths");
endfunction
