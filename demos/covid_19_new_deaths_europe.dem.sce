function f()
    [geoids, countries, timestamps, new_cases, new_deaths] = pandemic_covid_19_getdata();

    // only EU without Russia
    EU = ["Albania";"Andorra";"Armenia";"Austria";"Azerbaijan";"Belarus";
    "Belgium";"Bosnia_and_Herzegovina";"Bulgaria";"Croatia";"Cyprus";
    "Czechia";"Denmark";"Estonia";"Finland";"France";
    "Georgia";"Germany";"Greece";"Hungary";
    "Iceland";"Ireland";"Italy";"Kazakhstan";"Kosovo";
    "Latvia";"Liechtenstein";"Lithuania";"Luxembourg";"Malta";"Moldova";"Monaco";
    "Montenegro";"Netherlands";"North_Macedonia";"Norway";"Poland";
    "Portugal";"Romania";"San_Marino";"Serbia";"Slovakia";"Slovenia";
    "Spain";"Sweden";"Switzerland";"Turkey";"Ukraine";"United_Kingdom"];
             
    scf(41); clf();
    pandemic_plot_new_deaths(EU, countries, timestamps, new_deaths);
endfunction
f()
clear f
