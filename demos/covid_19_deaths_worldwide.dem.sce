function f()
    [geoids, countries, timestamps, new_cases, new_deaths] = pandemic_covid_19_getdata();

    // All Countries
    scf(20); clf()
    pandemic_plot_deaths([], countries, timestamps, new_deaths);
endfunction
f()
clear f
