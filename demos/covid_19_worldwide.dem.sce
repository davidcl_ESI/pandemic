function f()
    [geoids, countries, timestamps, new_cases, new_deaths] = pandemic_covid_19_getdata();

    // All Countries
    scf(10); clf();
    pandemic_plot_cases([], geoids, countries, timestamps, new_cases);
endfunction
f()
clear f
